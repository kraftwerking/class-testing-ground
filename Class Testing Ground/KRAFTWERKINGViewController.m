//
//  KRAFTWERKINGViewController.m
//  Class Testing Ground
//
//  Created by RJ Militante on 1/16/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "KRAFTWERKINGViewController.h"

@interface KRAFTWERKINGViewController ()

@end

@implementation KRAFTWERKINGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSString *myString = @"The Newfoundland dog has webbed feet which aids in its swimming prowress";
    NSArray *wordsInSentence = [myString componentsSeparatedByString:@" "];
    NSLog(@"%@", wordsInSentence);
    NSMutableArray *capitalizedWords = [[NSMutableArray alloc] init];
//    for (int word; word < [wordsInSentence count]; word++){
//        NSString *uncapitalizedWord = [wordsInSentence objectAtIndex:word];
//        NSString *capitalizedWord = [uncapitalizedWord capitalizedString];
//        [capitalizedWords addObject:capitalizedWord];
//        NSLog(@"%@", capitalizedWords);
//    }
    
    for(NSString *word in wordsInSentence)
    {
        NSString *capitalizedWord = [word capitalizedString];
        [capitalizedWords addObject:capitalizedWord];
        NSLog(@"%@", capitalizedWords);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
